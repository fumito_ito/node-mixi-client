var MixiClient, MixiSession, crypto, https, querystring;

https = require('https');

querystring = require('querystring');

crypto = require('crypto');

MixiSession = require('./MixiSession').MixiSession;

/*
# @class MixiClient
*/


MixiClient = (function() {
  /*
  	# @constructor
  	# @params {String} api_key
  	# @params {String} api_secret
  	# @params {Array} scopes
  	# @params {String} device -default 'pc'
  	# @params {Object} options - default empty object
  */

  function MixiClient(client_id, client_secret, device, options) {
    this.client_id = client_id;
    this.client_secret = client_secret;
    this.device = device != null ? device : 'pc';
    this.options = options != null ? options : {};
    if (!this.options.timeout) {
      this.options.timeout = 10000;
    }
    if (!this.options.mixi_graph_secure_server_host) {
      this.options.mixi_graph_secure_server_host = 'api.mixi-platform.com';
    }
    if (!this.options.mixi_graph_secure_server_port) {
      this.options.mixi_graph_secure_server_port = 443;
    }
    this.options.mixi_graph_server_path = '/2';
    this.options.mixi_authorization_host = 'mixi.jp';
    this.options.mixi_authorization_port = 443;
    this.options.mixi_authorization_path = '/connect_authorize.pl';
    this.options.mixi_token_host = 'secure.mixi-platform.com';
    this.options.mixi_token_port = 443;
    this.options.mixi_token_path = '/2/token';
  }

  /*
  	# options contain following paramters
  	# 	- method
  	# 		- 'GET' or 'POST'
  	# 	- host
  	# 		-
  	# 	- port
  	#   	- default: 80
  	#   - path
  	# 		- contain resource and query paramters when call 'GET' request
  	#     - contain only resource when call 'POST' request
  	#   - data
  	# 		- contain query paramters when call 'POST'
  	#   - timeout
  	#  		-
  	# @method doRequest
  	# @params {Object} options - default empty object
  	# @retrun {Function} Nameless function accept callback
  */


  MixiClient.prototype.doRequest = function(options, callback) {
    var has_returned, mark_as_retruned, req, req_option, timeout_timer;
    if (options == null) {
      options = {};
    }
    if (!options.host || !options.port || !options.path || !options.timeout) {
      throw new Error('Lack arguments parameter. host/port/path/timeout is required on doRequest.');
    }
    has_returned = false;
    timeout_timer = null;
    mark_as_retruned = function() {
      has_returned = true;
      if (timeout_timer) {
        return clearTimeout(timeout_timer);
      }
    };
    req_option = {
      host: options.host,
      port: options.port,
      path: options.path,
      method: options.method || 'GET'
    };
    req_option.headers = options.headers;
    req = https.request(req_option, function(res) {
      var data;
      res.setEncoding('utf8');
      data = [];
      res.on('data', function(chunk) {
        return data.push(chunk);
      });
      return res.on('end', function() {
        mark_as_retruned();
        return callback(null, data.join(''));
      });
    });
    req.on('error', function(err) {
      return callback(err, null);
    });
    if (options.data) {
      req.write(JSON.stringify(options.data));
    }
    req.end();
    return timeout_timer = setTimeout(function() {
      if (has_returned) {
        return;
      }
      mark_as_retruned();
      callback(new Error('Request timeout'), null);
      return req.abort();
    }, options.timeout);
  };

  /*
  	# TODO: contentURI is not counted
  	# @method graphCall
  	# @params {String} resource
  	# @params {Object} params - default empty object
  	# @params {String} method
  	# @params {Object} options - default empty object
  	# @return {Function} doRequest
  */


  MixiClient.prototype.graphCall = function(resource, params, callback, method, options) {
    if (params == null) {
      params = {};
    }
    if (method == null) {
      method = 'GET';
    }
    if (options == null) {
      options = {};
    }
    params.format = 'json';
    params.count = params.count || 500;
    if (!options.host) {
      options.host = this.options.mixi_graph_secure_server_host;
    }
    if (!options.port) {
      options.port = this.options.mixi_graph_secure_server_port;
    }
    options.path = this.options.mixi_graph_server_path + resource;
    if (!options.timeout) {
      options.timeout = this.options.timeout;
    }
    options.method = method;
    options.headers = {
      'Authorization': 'Bearer ' + params.access_token
    };
    delete params.access_token;
    if (method === 'POST') {
      options.headers['Content-Type'] = 'application/json';
      options.data = params;
    } else {
      options.path += '?' + querystring.stringify(params);
    }
    return this.doRequest(options, callback);
  };

  /*
  	# @method refreshAccessToken
  	# @params {Object} params
  	# @params {Function} callback
  	# @return {Function} MixiClient#doRequest
  */


  MixiClient.prototype.refreshAccessToken = function(params, callback) {
    var options;
    params.format = 'json';
    options = {
      host: this.options.mixi_token_host,
      port: this.options.mixi_token_port,
      path: this.options.mixi_token_path,
      timeout: this.options.timeout,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    options.path += '?' + querystring.stringify(params);
    return this.doRequest(options, callback);
  };

  /*
  	# @method getSessionByTokens
  	# @params {String} access_token
  	# @params {String} refresh_token
  	# @params {String} authorization_code
  	# @params {Function} callback : callback will catch MixiSession Oject
  	# @return {Function} callback
  */


  MixiClient.prototype.getSessionByTokens = function(access_token, refresh_token, authorization_code, callback) {
    var session;
    session = new MixiSession(this, access_token, refresh_token, authorization_code);
    return callback(session);
  };

  return MixiClient;

})();

exports.MixiClient = MixiClient;
