/*
# @class MixiSession
*/

var MixiSession;

MixiSession = (function() {
  /*
  	@constructor
  */

  function MixiSession(mixi_client, access_token, refresh_token, authorization_code) {
    this.mixi_client = mixi_client;
    this.access_token = access_token;
    this.refresh_token = refresh_token;
    this.authorization_code = authorization_code;
  }

  /*
  	@method refreshAccessToken
  */


  MixiSession.prototype.refreshAccessToken = function(refresh_token, callback) {
    var params,
      _this = this;
    params = {
      'grant_type': 'refresh_token',
      'client_id': this.mixi_client.client_id,
      'client_secret': this.mixi_client.client_secret,
      'refresh_token': this.refresh_token
    };
    return this.mixi_client.refreshAccessToken(params, function(err, result) {
      var json;
      if (err) {
        if (callback) {
          return callback(err, null);
        } else {
          throw err;
        }
      } else {
        json = JSON.parse(result);
        _this.access_token = json.access_token;
        _this.refresh_token = json.refresh_token;
        if (callback) {
          return callback(null, result);
        }
      }
    });
  };

  /*
  	@method graphCall
  */


  MixiSession.prototype.graphCall = function(resource, params, callback, method, options) {
    var _this = this;
    return this.refreshAccessToken(this.refresh_token, function(err, result) {
      if (err) {
        throw err;
      }
      params['access_token'] = _this.access_token;
      return _this.mixi_client.graphCall(resource, params, callback, method, options);
    });
  };

  return MixiSession;

})();

exports.MixiSession = MixiSession;
