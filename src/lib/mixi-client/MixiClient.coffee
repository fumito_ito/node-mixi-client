https = require('https')
querystring = require('querystring')
crypto = require('crypto')

MixiSession = require('./MixiSession').MixiSession
###
# @class MixiClient
###
class MixiClient
	###
	# @constructor
	# @params {String} api_key
	# @params {String} api_secret
	# @params {Array} scopes
	# @params {String} device -default 'pc'
	# @params {Object} options - default empty object
	###
	constructor: (@client_id, @client_secret, @device = 'pc', @options = {}) ->
		@options.timeout = 10000 if not @options.timeout
		# must add path /2/{graph}
		# Graph API
		@options.mixi_graph_secure_server_host = 'api.mixi-platform.com' if not @options.mixi_graph_secure_server_host
		@options.mixi_graph_secure_server_port = 443 if not @options.mixi_graph_secure_server_port
		@options.mixi_graph_server_path = '/2'
		@options.mixi_authorization_host = 'mixi.jp'
		@options.mixi_authorization_port = 443
		@options.mixi_authorization_path = '/connect_authorize.pl'
		@options.mixi_token_host = 'secure.mixi-platform.com'
		@options.mixi_token_port = 443
		@options.mixi_token_path = '/2/token'

	###
	# options contain following paramters
	# 	- method
	# 		- 'GET' or 'POST'
	# 	- host
	# 		-
	# 	- port
	#   	- default: 80
	#   - path
	# 		- contain resource and query paramters when call 'GET' request
	#     - contain only resource when call 'POST' request
	#   - data
	# 		- contain query paramters when call 'POST'
	#   - timeout
	#  		-
	# @method doRequest
	# @params {Object} options - default empty object
	# @retrun {Function} Nameless function accept callback
	###
	doRequest: (options = {}, callback) ->
		if not options.host or not options.port or not options.path or not options.timeout
			throw new Error 'Lack arguments parameter. host/port/path/timeout is required on doRequest.'

		has_returned = false
		timeout_timer = null

		mark_as_retruned = ->
			has_returned = true
			if timeout_timer
				clearTimeout timeout_timer

		req_option =
			host: options.host
			port: options.port
			path: options.path
			method: options.method or 'GET'

		req_option.headers = options.headers

		# HTTP Request
		req = https.request req_option, (res) ->
			res.setEncoding 'utf8'
			data = []
			res.on 'data', (chunk) ->
				data.push chunk
			res.on 'end', ->
				mark_as_retruned()
				callback null, data.join('')
		# Request error handler
		req.on 'error', (err) ->
			callback err, null

		if options.data
			req.write JSON.stringify options.data

		req.end()

		timeout_timer = setTimeout ->
			return if has_returned
			mark_as_retruned()
			callback new Error('Request timeout'), null
			req.abort()
		, options.timeout

	###
	# TODO: contentURI is not counted
	# @method graphCall
	# @params {String} resource
	# @params {Object} params - default empty object
	# @params {String} method
	# @params {Object} options - default empty object
	# @return {Function} doRequest
	###
	graphCall: (resource, params = {}, callback, method = 'GET', options = {}) ->
		# response format should be JSON
		params.format = 'json'
		params.count = params.count || 500
		# be secure!
		options.host = @options.mixi_graph_secure_server_host if not options.host
		options.port = @options.mixi_graph_secure_server_port if not options.port
		options.path = @options.mixi_graph_server_path + resource
		options.timeout = @options.timeout if not options.timeout
		options.method = method
		options.headers =
			'Authorization': 'Bearer ' + params.access_token
		delete params.access_token

		if method is 'POST'
			options.headers['Content-Type'] = 'application/json'
			options.data = params
		else
			#options.headers['Content-Type'] = 'application/x-www-form-urlencoded'
			options.path += '?' + querystring.stringify params

		@doRequest options, callback

	###
	# @method refreshAccessToken
	# @params {Object} params
	# @params {Function} callback
	# @return {Function} MixiClient#doRequest
	###
	refreshAccessToken: (params, callback) ->
		#
		params.format = 'json'
		options =
			host: @options.mixi_token_host
			port:  @options.mixi_token_port
			path:  @options.mixi_token_path
			timeout:  @options.timeout
			method:  'POST'
			headers:
				'Content-Type': 'application/x-www-form-urlencoded'

		options.path += '?' + querystring.stringify params

		@doRequest options, callback

	###
	# @method getSessionByTokens
	# @params {String} access_token
	# @params {String} refresh_token
	# @params {String} authorization_code
	# @params {Function} callback : callback will catch MixiSession Oject
	# @return {Function} callback
	###
	getSessionByTokens: (access_token, refresh_token, authorization_code, callback) ->
		session = new MixiSession @, access_token, refresh_token, authorization_code
		callback(session)

exports.MixiClient = MixiClient