###
# @class MixiSession
###
class MixiSession
	###
	@constructor
	###
	constructor: (@mixi_client, @access_token, @refresh_token, @authorization_code) ->

	###
	@method refreshAccessToken
	###
	refreshAccessToken: (refresh_token, callback) ->
		params =
			'grant_type': 'refresh_token'
			'client_id': @mixi_client.client_id
			'client_secret': @mixi_client.client_secret
			'refresh_token': @refresh_token

		@mixi_client.refreshAccessToken params, (err, result) =>
			if err
				if callback then callback(err, null) else throw err
			else
				json = JSON.parse result
				@access_token = json.access_token
				@refresh_token = json.refresh_token
				callback null, result if callback

	###
	@method graphCall
	###
	graphCall: (resource, params, callback, method, options) ->
		@refreshAccessToken @refresh_token, (err, result) =>
			throw err if err
			params['access_token'] = @access_token
			@mixi_client.graphCall resource, params, callback, method, options


exports.MixiSession = MixiSession