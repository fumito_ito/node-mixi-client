/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    watch: {
      files: 'src/**/*.coffee',
      tasks: 'coffee'
    },
    coffee: {
      index: {
        src: 'src/index.coffee',
        dest: '.',
        options: {
          bare: true
        }
      },
      lib: {
        src: 'src/lib/mixi-client/*.coffee',
        dest: './lib/mixi-client',
        options: {
          bare: true
        }
      },
      test: {
        src: 'src/test/*.coffee',
        dest: 'test',
        options: {
          bare: true
        }
      }
    },
    yuidoc: {
      dist: {
        name: 'node-mixi-client',
        description: '',
        version: '0.0.1',
        url: 'https://bitbucket.org/fumito_ito/node-mixi-client',
        options: {
          paths: './src',
          outdir: './doc',
          syntaxtype: 'coffee',
          extension: '.coffee'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-yuidoc');
  grunt.loadNpmTasks('grunt-coffee');
  grunt.loadNpmTasks('grunt-simple-mocha');
  // Default task.
  grunt.registerTask('default', 'coffee');

};
